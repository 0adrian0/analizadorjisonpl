###ETSII ULL Grado de Informática-Asignatura: Procesadores de Lenguajes
###Práctica 7: Analizador de PL0 Usando Jison

## Descripción
Reescribir el analizador del lenguaje PL0 realizado en las práctica anteriores utilizando Jison.

##Desarrollo

Los lenguajes y herramientas más utilizados para el desarrollo del presente proyecto fueron: 

  * Heroku
  * Sinatra
  * Ruby
  * Jison
  * JavaScript
  * Oauth
  * DataMaper

## Enlaces

[Despliegue en Heroku](http://analizador-jison.herokuapp.com)

[Test](http://analizador-jison.herokuapp.com/tests)

## Autores

  - Adrián González Martín [Ir a perfil](https://github.com/alu4073)
  - Sara Martín Molina [Ir a perfil](https://github.com/alu4102)

## Licencia

Este proyecto se distribuye bajo la licencia MIT. Para saber más, leer el fichero ['licenciaMIT']