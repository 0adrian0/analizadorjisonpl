/* description: Parses end executes mathematical expressions. */

%{
var symbol_table = {};

function fact (n) { 
  return n==0 ? 1 : fact(n-1) * n 
}

var tree = function(f, r) {
    if (r.length > 0) {
      var last = r.pop();
      var result = {
        type:  last[0],
        left: tree(f, r),
        right: last[1]
      };
    }
    else {
      var result = f;
    }
    return result;
}

%}

%token NUMBER COMPARISION ID E PI EOF PROCEDURE LEFTPAR RIGHTPAR COLON SEMICOLON
/* operator associations and precedence */

%right '='
%left '+' '-'
%left '*' '/'
%left '^'
%right '%'
%left UMINUS
%left '!'

%start prog

%% /* language grammar */

/* ------------------------ */


prog
    : consts vars block DOT EOF
        { 
          if (($1 != null) && ($2 != null)) {
            $$ = [$1, $2, $3]; 
            return $$;
          }
          if ($1 != null) {
            $$ = [$1, $3];
            return $$; 
          } 
          if ($2 != null) {
            $$ = [$2, $3]; 
            return $$;
          }
          if (($1 == null) && ($2 == null)) {
            $$ = $3; 
            return $$;
          }
        }
    ;

consts
    : CONST constants ';' consts
        { 
          if ($4) $$ = {constants: $2, constants: $4};
          else $$ = {constants: $2};
        }
    | /* empty */
    ;

vars
    : VAR identificators ';' vars
        { 
          if ($4) $$ = {variables: $2, variables: $4};
          else $$ = {variables: $2};
        }
    | /* empty */
    ; 

block 
    : process statements
        { 
          $$ = {procedures: $1, statements: $2}; 
        }
    | statements
        { 
          $$ = $1; 
        }
    ;


process
    : PROCEDURE ID '(' variables ')' COLON block END ';'
      { $$ = [ "process", {name: $2}, {parameters: $4}, {content: $7}]; }
    | PROCEDURE ID COLON block END ';'
      { $$ = [ "process", {name: $2}, {content: $4}]; }
    | process PROCEDURE ID '(' variables ')' COLON block END ';'
      { 
          $$ = $1;
          if ($3) $$.push(["process", {name: $3}, {parameters: $5}, {content: $8}]); 
      }
    | process PROCEDURE ID COLON block END ';'
      { 
          $$ = $1;
          if ($3) $$.push(["process", {name: $3}, {content: $5}]); 
      }
    ;

variables
    : ID
      { $$ =  (typeof $1 === 'undefined')? [] : [{var: $1}]; }
    | variables COMMA ID
      { 
         $$ = $1;
         if ($3) $$.push({var: $3}); 
      }
    ;

constants 
    : ID '=' NUMBER
      { $$ =  (typeof $1 === 'undefined')? [] : [{name: $1, value: $3}]; }
    | constants COMMA ID '=' NUMBER
      { 
         $$ = $1;
         if ($3) $$.push({name: $3, value: $5}); 
      }
    ;

identificators
    : ID
      { $$ =  (typeof $1 === 'undefined')? [] : [{name: $1}]; }
    | identificators COMMA ID
      { 
         $$ = $1;
         if ($3) $$.push({name: $3}); 
      }
    ;

statements
    : ID '=' expression 
      { $$ = ["ASSIGN", ["left:", {ID: $1}, "right:", $3]];}
    | ID '=' expression statements
      { 
        $$ = ["ASSIGN", ["left:", {ID: $1}, "right:", $3]];
        if ($4) $$.push($4); 
      }
    | ID '=' expression ';'
      { $$ = ["ASSIGN", ["left:", {ID: $1}, "right:", $3]];}
    | ID '=' expression ';' statements
      { 
        $$ = ["ASSIGN", ["left:", {ID: $1}, "right:", $3]];
        if ($5) $$.push($5); 
      }
    | IF condition THEN statements ELSE statements END ';'
       { $$ = ["If-Then-Else", [{condition: $2 }, "left:", $4, "right", $6]];}
    | IF condition THEN statements ELSE statements END ';' statements
       { 
         $$ = ["If-Then-Else", [{condition: $2 }, "left:", $4, "right", $6]];
         if ($9) $$.push($9); 
       }
    | IF condition THEN statements END ';'
       { $$ = ["If-Then", [{condition: $2 }, "statements:", $4]];}
    | IF condition THEN statements END ';' statements
       { 
         $$ = ["If-Then", [{condition: $2 }, "statements:", $4]];
         if ($7) $$.push($7); 
       }
    | CALL ID '(' variables ')' ';' 
      {  $$ = ["CALL", [{procedure: $2, arguments: $4}]];} 
    | CALL ID '(' variables ')' ';' statements
      { 
        $$ = ["CALL", [{procedure: $2, arguments: $4}]];
        if ($7) $$.push($7); 
      } 
    | CALL ID ';' 
      {  $$ = ["CALL", [{procedure: $2}]];} 
    | CALL ID ';' statements
      { 
        $$ = ["CALL", [{procedure: $2}]];
        if ($4) $$.push($4); 
      }
    | P expression ';'
      {  $$ = ["P", [{expresion: $2}]];}
    | P expression ';' statements
      {  
         $$ = ["P", [{expresion: $2}]];
         if ($4) $$.push($4); 
      } 
    | WHILE condition DO statements END ';'
      {  $$ = ["WHILE", [{condition: $2 }, "statements:", $4]]; }
    | WHILE condition DO statements END ';' statements
      { 
         $$ = ["WHILE", [{condition: $2 }, "statements:", $4]];
         if ($7) $$.push($7); 
      }
    | BEGIN statements END ';'
      {  $$ = ["BEGIN-END", ["statements:", $2]]; }
    | BEGIN statements END ';' statements
      {  
         $$ = ["BEGIN-END", ["statements:", $2]]; 
         if ($5) $$.push($5); 
      }
    ;


condition
    : ODD expression
      { $$ = [{odd: $2 }];}
    | expression COMPARISION expression
      { $$ = [{comparision: $2 }, "left", $1, "right", $3];}
    ;


expression
    : expression '+' expression
        {$$ = ["+", ["left:", $1, "right:", $3]]; }
    | expression '-' expression
        {$$ = ["-", ["left:", $1, "right:", $3]];}
    | expression '*' expression
        {$$ = ["*", ["left:", $1, "right:", $3]];}
    | expression '/' expression
        {
          if ($3 == 0) throw new Error("Division by zero, error!");
          $$ = ["/", ["left:", $1, "right:", $3]];
        }
    | expression '^' expression
        {$$ = ["^", ["left:", $1, "right:", $3]];}
    | expression '!'
        {
          if ($1.number % 1 !== 0) 
             throw "Error! Attempt to compute the factorial of "+
                   "a floating point number "+$1;
          $$ = ["!", $1];
        }
    | expression '%'
        {$$ = ["%", $1];}
    | '-' expression %prec UMINUS
        {$$ = -$2;}
    | '(' expression ')'
        {$$ = $2;}
    | NUMBER
        {
          $$ = {number: Number(yytext)};
        }
    | E
        {$$ = Math.E;}
    | PI
        {$$ = Math.PI;}
    | ID 
        {$$ = symbol_table[yytext] || {variable: yytext} }
    ;



